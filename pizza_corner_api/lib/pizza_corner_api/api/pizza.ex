defmodule PizzaCornerApi.Api.Pizza do
  use Ecto.Schema
  import Ecto.Changeset

  schema "pizzas" do
    field :description, :string
    field :image, :string
    field :name, :string

    timestamps()
  end

  @doc false
  def changeset(pizza, attrs) do
    pizza
    |> cast(attrs, [:name, :description, :image])
    |> validate_required([:name, :description, :image])
  end
end
