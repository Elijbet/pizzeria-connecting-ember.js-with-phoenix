defmodule PizzaCornerApi.Repo do
  use Ecto.Repo,
    otp_app: :pizza_corner_api,
    adapter: Ecto.Adapters.Postgres
end
