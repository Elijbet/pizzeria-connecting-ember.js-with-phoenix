defmodule PizzaCornerApiWeb.PizzaView do
  use PizzaCornerApiWeb, :view
  use JaSerializer.PhoenixView

  attributes [:name, :description, :image]
end
