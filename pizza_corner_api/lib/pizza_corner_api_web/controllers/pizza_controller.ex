defmodule PizzaCornerApiWeb.PizzaController do
  use PizzaCornerApiWeb, :controller

  alias PizzaCornerApi.Repo
  alias PizzaCornerApi.Api.Pizza

  def index(conn, _params) do
    render conn, "index.json-api", data: Repo.all(Pizza)
  end
end
