defmodule PizzaCornerApi.Repo.Migrations.CreatePizzas do
  use Ecto.Migration

  def change do
    create table(:pizzas) do
      add :name, :string
      add :description, :text
      add :image, :string

      timestamps()
    end

  end
end
