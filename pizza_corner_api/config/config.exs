# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :pizza_corner_api,
  ecto_repos: [PizzaCornerApi.Repo]

# Configures the endpoint
config :pizza_corner_api, PizzaCornerApiWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "4svhC7pk2sCGi0kYASAnidyLFZmgHfr6FNh6TliVc3eJ3AwyeetFaWJKrpo8eXr2",
  render_errors: [view: PizzaCornerApiWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: PizzaCornerApi.PubSub,
  live_view: [signing_salt: "WHt3b9gR"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"

# Installing JaSerializer so that that Phoenix and Ember apps can send and receive JSONAPI responses.

config :phoenix, :format_encoders,
  "json-api": Poison
# The MIME media type for JSON text (.json) is application/json. MIME types can be extended in your application configuration as follows:
config :mime, :types, %{
  "application/vnd.api+json" => ["json-api"]
}
# After adding the configuration, MIME needs to be recompiled. If you are using mix, it can be done with:
# $ mix deps.clean mime --build
# $ mix deps.get
